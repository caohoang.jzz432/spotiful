This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Coding Convention

For better clarity and coding experience, components should be named as:

- Plain React Component: 'export const ReactComponent'
- Styled React Component: 'export const SReactComponent' (Notify the 'S' letter)

## Required Extensions for Better Coding Experience

EditorConfig for VS Code (to have seamlessly experience between dev's computer when using VS Code): https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig

Prettier ESLint (to format your code according to .eslintrc.json): https://marketplace.visualstudio.com/items?itemName=rvest.vs-code-prettier-eslint

ESLint (to integrate ESLint with VS Code): https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint

vscode-styled-components: https://marketplace.visualstudio.com/items?itemName=jpoissonnier.vscode-styled-components

eslint-plugin-import (to stop you from bad import): https://github.com/import-js/eslint-plugin-import

Trouble with Husky? Run "npx husky install" to install git hook (https://stackoverflow.com/a/66356366). Why? Because everyone recommends v4.

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.tsx`. The page auto-updates as you edit the file.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!
