/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
  eslint: {
    dirs: [
      'components',
      'constants',
      'contexts',
      'hooks',
      'pages',
      'redux',
      'types',
      'utils',
    ],
  },
  env: {
    APP_NAME: 'Spotiful',
  },
};
