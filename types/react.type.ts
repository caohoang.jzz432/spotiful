import React from 'react';

export interface BaseProps {
  children?: React.ReactNode[] | React.ReactNode;
  className?: string;
}
