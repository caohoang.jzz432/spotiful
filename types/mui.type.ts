import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import { MUIStyledCommonProps, Theme } from '@mui/system';
import {
  align,
  borderRadius,
  color,
  fontSize,
  fontWeight,
  imageSize,
  shadow,
} from 'styles/themes/theme.const';

declare module '@mui/material/styles' {
  type Theme = CustomThemeOptions;
  // allow configuration using `createTheme`
  type ThemeOptions = CustomThemeOptions;
}

/**
 * Types for theme properties
 */
export type Color = typeof color;

export type FontSize = typeof fontSize;

export type FontWeight = typeof fontWeight;

export type BorderRadius = typeof borderRadius;

export type ImageSize = typeof imageSize;

export type Align = typeof align;

export type Shadow = typeof shadow;

/**
 * Key types for theme properties
 */
export type ColorKey = keyof Color;

export type FontSizeKey = keyof FontSize;

export type FontWeightKey = keyof FontWeight;

export type BorderRadiusKey = keyof BorderRadius;

export type ImageSizeKey = keyof ImageSize;

export type AlignKey = keyof Align;

export type ShadowKey = keyof Shadow;

export interface CustomThemeOptions extends Theme {
  color: Color;
  fontSize: FontSize;
  fontWeight: FontWeight;
  borderRadius: BorderRadius;
  imageSize: ImageSize;
  align: Align;
  shadow: Shadow;
}

export interface OverriddenMUIStyledCommonProps extends MUIStyledCommonProps {
  theme?: CustomThemeOptions;
  className?: string;
}

export type Icon = typeof PlayArrowIcon;
