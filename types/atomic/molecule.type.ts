/**
 * ----------------------------------------
 * Only atomic components' types goes here!
 * DO NOT put any styled-related types here!
 * ----------------------------------------
 */

import { BaseProps } from 'types/react.type';

/**
 * SongBox
 */
export interface SongBoxData {
  index: number;
  imageCover: StaticImageData;
  songName: string;
  artist: string;
  genre: string;
  duration: number; // in seconds
}

export interface SongBoxProps extends BaseProps {
  data: SongBoxData[];
}

/**
 * RecentPlayedBox
 */
export interface RecentPlayedBoxProps extends BaseProps {
  title: string;
}

/**
 * InterestBox
 */
export interface InterestBoxProps extends BaseProps {
  title: string;
}

/**
 * MenuGroup
 */
export interface MenuGroupProps extends BaseProps {
  title: string;
}

/**
 * GoodBox
 */
export interface GoodBoxData {
  imageCover: StaticImageData | string;
  title: string;
}
