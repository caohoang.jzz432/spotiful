/**
 * ----------------------------------------
 * Only atomic components' types goes here!
 * DO NOT put any styled-related types here!
 * ----------------------------------------
 */

import { ReactElement } from 'react';
import { ColorKey } from 'types/mui.type';
import { BaseProps } from 'types/react.type';

/**
 * MenuLink
 */
export interface MenuLinkProps extends BaseProps {
  // eslint-disable-next-line @typescript-eslint/ban-types
  icon: ReactElement;
  title: string;
  active?: boolean;
}

/**
 * MusicSlider
 */
export interface MusicSliderProps extends BaseProps {
  duration?: number; // in seconds
  thumbColor?: string;
}

/**
 * SquareButton
 */
export interface SquareButtonProps extends BaseProps {
  icon: ReactElement;
  color: ColorKey;
  absolute?: boolean;
  border?: boolean;
}

/**
 * SongCard
 */
export interface SongCardProps {
  songName: string;
  artist: string;
}

export interface SongCardData {
  songName: string;
  artist: string;
}
