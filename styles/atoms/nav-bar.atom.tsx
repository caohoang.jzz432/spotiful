import {styled } from '@mui/system';

export const SNavBar = styled('div')({
  position: 'relative',
  borderRight: '1px solid #eaf0fb',
  height: '100vh',
})