import { styled } from '@mui/system';

export const SSSearchBox = styled('div')({
  display: 'flex',
  justifyContent: 'center',
  gap: '20px 0',
  padding: '0 40px',
});
