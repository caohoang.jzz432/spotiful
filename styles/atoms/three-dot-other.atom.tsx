import { styled } from '@mui/system';
import { OverriddenMUIStyledCommonProps } from 'types/mui.type';

export const SThreeDotOther = styled('div')(
  ({ theme }: OverriddenMUIStyledCommonProps) => ({
    display: 'flex',
    gap: '0 5px',
    color: theme?.color.lightgrey,
    alignItems: 'center',
    cursor: 'pointer',
  }),
);
