import { styled } from '@mui/system';
import { OverriddenMUIStyledCommonProps } from 'types/mui.type';

interface SMenuLinkProps extends OverriddenMUIStyledCommonProps {
  $active?: boolean;
}

export const SMenuLink = styled('div')(
  ({ theme, $active }: SMenuLinkProps) => ({
    display: 'flex',
    alignItems: 'center',
    gap: '0 15px',
    color: $active ? theme?.color.orange : 'inherit',
    '&:hover': {
      color: theme?.color.orange,
      borderRight: `5px solid ${theme?.color.orange || '#000'}`,
      cursor: 'pointer',
      '& .menu-icon': {
        color: theme?.color.orange,
      },
    },
  }),
);
