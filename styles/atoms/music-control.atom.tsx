import { styled } from '@mui/system';
import { OverriddenMUIStyledCommonProps } from '../../types/mui.type';
import { customStyledOptions } from '../themes/global.theme';

export const SMusicControlContainer = styled('div')({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  gap: '0 15px',
});

interface SMusicControlProps extends OverriddenMUIStyledCommonProps {
  $active?: boolean;
}

export const SMusicControl = styled(
  'div',
  customStyledOptions,
)(({ theme, $active }: SMusicControlProps) => ({
  color: '#fff',
  backgroundColor: $active ? theme?.color.whitetransparent : 'transparent',
  '&:hover': {
    backgroundColor: theme?.color.whitetransparent,
  },
  borderRadius: theme?.borderRadius.card,
  padding: '10px',
  cursor: 'pointer',
}));
