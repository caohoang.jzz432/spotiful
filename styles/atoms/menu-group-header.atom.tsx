import { styled } from '@mui/system';
import { OverriddenMUIStyledCommonProps } from 'types/mui.type';

export const SMenuGroupHeader = styled('div')(
  ({ theme }: OverriddenMUIStyledCommonProps) => ({
    letterSpacing: '2px',
    color: theme?.color.lightgrey,
  }),
);
