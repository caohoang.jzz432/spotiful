import { styled } from '@mui/system';
import { customStyledOptions } from 'styles/themes/global.theme';
import {
  AlignKey,
  ColorKey,
  FontSizeKey,
  FontWeightKey,
  OverriddenMUIStyledCommonProps,
} from 'types/mui.type';

interface TypographyProps extends OverriddenMUIStyledCommonProps {
  $size?: FontSizeKey;
  $color?: ColorKey;
  $weight?: FontWeightKey;
  $longText?: boolean;
  $anchor?: boolean;
  $align?: AlignKey;
}

export const STypography = styled(
  'span',
  customStyledOptions,
)(
  ({
    theme,
    $size = 'normal',
    $color = 'bluegrey',
    $weight = 'medium',
    $longText = false,
    $anchor = false,
    $align = 'left',
  }: TypographyProps) => {
    let css: any = {
      color: theme?.color[$color],
      fontSize: theme?.fontSize[$size],
      fontWeight: theme?.fontWeight[$weight],
      textAlign: theme?.align[$align],
    };

    if ($longText) {
      css = {
        ...css,
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
      };
    }

    if ($anchor) {
      css = {
        ...css,
        cursor: 'pointer',
      };
    }

    return css;
  },
);
