import { styled } from '@mui/system';

export const SNavBarCopyright = styled('div')({
  position: 'absolute',
  bottom: '35px',
  fontWeight: 400,
});
