import { styled } from '@mui/system';

export const SLogo = styled('div')({
  display: 'flex',
  alignItems: 'center',
  gap: '0 15px',
});
