import { styled } from '@mui/system';
import { OverriddenMUIStyledCommonProps } from 'types/mui.type';

export const SPickArtist = styled('div')(
  ({ theme }: OverriddenMUIStyledCommonProps) => ({
    display: 'flex',
    flexDirection: 'column',
    gap: '20px 0',
    width: '30%',
    padding: '15px',
    borderRadius: theme?.borderRadius.card,
    border: `1px solid ${theme?.color.greyborder || ''}`,
    alignItems: 'center',
    background: 'linear-gradient(to right, #cb2d3e, #ef473a)',
  }),
);
