import { ColorKey, OverriddenMUIStyledCommonProps } from 'types/mui.type';
import { styled } from '@mui/system';
import { Box } from '@mui/material';

interface SSquareButtonProps extends OverriddenMUIStyledCommonProps {
  $color: ColorKey;
  $absolute: boolean;
  $border: boolean;
}

export const SSquareButton = styled(Box)(
  ({ theme, $color, $absolute, $border }: SSquareButtonProps) => {
    let css: any = {
      color: theme?.color[$color],
      background: '#fff',
      cursor: 'pointer',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      height: 30,
      width: 30,
      bottom: 20,
      right: 10,
      borderRadius: theme?.borderRadius.card,
      border: `1px solid ${
        $border ? theme?.color.greyborder || '' : 'transparent'
      }`,
    };

    if ($absolute) {
      css = {
        ...css,
        position: 'absolute',
      };
    }

    return css;
  },
);
