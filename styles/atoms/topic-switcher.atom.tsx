import { styled } from '@mui/system';
import { OverriddenMUIStyledCommonProps } from 'types/mui.type';

export const STopicSwitcher = styled('div')(({ theme }: OverriddenMUIStyledCommonProps) => ({
  display: 'flex',
  gap: '0px 20px',
  '&': {
    '& .anchor': {
      textDecoration: 'none',
      color: theme?.color.lightgrey,
    },
    '& .anchor:active,.anchor:hover': {
      color: theme?.color.bluegrey || '#000',
    },
  },
}));
