import { styled } from '@mui/system';
import { customStyledOptions } from 'styles/themes/global.theme';
import { OverriddenMUIStyledCommonProps } from 'types/mui.type';

interface SMusicSliderProps extends OverriddenMUIStyledCommonProps {
  $thumbColor: string;
}

export const SMusicSlider = styled(
  'div',
  customStyledOptions,
)(({ theme, $thumbColor }: SMusicSliderProps) => ({
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  gap: '0 20px',
  width: '100%',

  '.MuiSlider-rail': {
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
  },

  '.MuiSlider-track': {
    backgroundColor: theme?.color.white,
  },

  '.MuiSlider-thumb': {
    border: `2px solid ${theme?.color.white || ''}`,
    color: $thumbColor,

    '&:hover': {
      boxShadow: '0px 0px 0px 8px rgb(255 255 255 / 16%)',
    },

    '&.Mui-focusVisible': {
      boxShadow: '0px 0px 0px 8px rgb(255 255 255 / 16%)',
    },
  },
}));
