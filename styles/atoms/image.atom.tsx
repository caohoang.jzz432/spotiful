import { styled } from '@mui/system';
import { customStyledOptions } from 'styles/themes/global.theme';
import {
  BorderRadiusKey,
  ImageSizeKey,
  OverriddenMUIStyledCommonProps,
} from 'types/mui.type';

interface SStrictImageProps extends OverriddenMUIStyledCommonProps {
  $size?: ImageSizeKey;
  $border?: BorderRadiusKey;
  $anchor?: boolean;
  $autoFill?: boolean;
}

/**
 * Image that has its width equals to its height (perfect square)
 */
export const SStrictImageWrapper = styled(
  'div',
  customStyledOptions,
)(
  ({
    theme,
    $size = 'normal',
    $border = 'card',
    $anchor,
    $autoFill,
  }: SStrictImageProps) => {
    let css: any = {
      borderRadius: theme?.borderRadius[$border],
      width: theme?.imageSize[$size],
      height: theme?.imageSize[$size],
      '& > span': {
        borderRadius: 'inherit',
      },
    };

    if ($anchor) {
      css = {
        ...css,
        cursor: 'pointer',
      };
    }

    if ($autoFill) {
      css = {
        ...css,
        width: 'fit-content',
        height: 'fit-content',
        position: 'relative',
      };
    }

    return css;
  },
);

interface SImageProps extends OverriddenMUIStyledCommonProps {
  $width: number | string;
  $height: number | string;
  $border: BorderRadiusKey;
}

/**
 * Image that the user could define its height and width
 */
export const SImageWrapper = styled(
  'div',
  customStyledOptions,
)(({ theme, $width, $height, $border }: SImageProps) => ({
  borderRadius: $border === 'none' ? 'none' : theme?.borderRadius[$border],
  width: $width,
  height: $height,
}));
