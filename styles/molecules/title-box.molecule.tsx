import { Box, styled } from '@mui/system';

export const STitleBox = styled(Box)({
  display: 'flex',
  justifyContent: 'space-between',
});
