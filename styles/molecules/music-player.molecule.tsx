import { styled } from '@mui/system';
import { customStyledOptions } from 'styles/themes/global.theme';
import { OverriddenMUIStyledCommonProps } from 'types/mui.type';

interface SMusicPlayerProps extends OverriddenMUIStyledCommonProps {
  $bgcolor: string;
}

export const SMusicPlayer = styled(
  'div',
  customStyledOptions,
)(({ theme, $bgcolor }: SMusicPlayerProps) => ({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  gap: '20px 0',
  backgroundColor: $bgcolor,
  padding: '30px',
  borderRadius: theme?.borderRadius.card,
}));
