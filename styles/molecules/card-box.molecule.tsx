import { Box } from '@mui/material';
import { styled } from '@mui/system';

export const SCardBox = styled(Box)({
  display: 'grid',
  gap: '20px 20px',
  gridTemplateColumns: 'repeat(auto-fill, minmax(160px, 1fr))',
});
