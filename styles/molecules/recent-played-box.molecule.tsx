import { styled } from '@mui/system';
import { OverriddenMUIStyledCommonProps } from 'types/mui.type';

export const SRecentPlayedBox = styled('div')(
  ({ theme }: OverriddenMUIStyledCommonProps) => ({
    display: 'flex',
    flexDirection: 'column',
    gap: '20px 0',
    padding: '15px',
    borderRadius: theme?.borderRadius.card,
    border: `1px solid ${theme?.color.greyborder || ''}`,
    width: '100%',

    '& .play-button:hover,.add-button:hover': {
      boxShadow: theme?.shadow.shadowHover,
    },
  }),
);
