import { styled } from '@mui/system';

export const STopResult = styled('div')({
  display: 'flex',
  gap: '0 20px',
});
