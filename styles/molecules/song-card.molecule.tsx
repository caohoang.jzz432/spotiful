import { styled } from '@mui/system';
import { OverriddenMUIStyledCommonProps } from 'types/mui.type';

export const SSongCard = styled('div')(
  ({ theme }: OverriddenMUIStyledCommonProps) => ({
    display: 'flex',
    flexDirection: 'column',
    gap: '5px 0',
    border: `1px solid ${theme?.color.greyborder || ''}`,
    borderRadius: theme?.borderRadius.card,
    padding: '15px',
    position: 'relative',

    '&:hover': {
      boxShadow: theme?.shadow.shadowHover,
    },
  }),
);
