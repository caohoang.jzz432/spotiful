import { styled } from '@mui/system';

export const SMenuGroup = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  gap: '20px 0',
});
