import { styled } from '@mui/system';
import { OverriddenMUIStyledCommonProps } from 'types/mui.type';

export const SLayout = styled('div')(
  ({ theme }: OverriddenMUIStyledCommonProps) => ({
    display: 'grid',
    gridTemplateColumns: '240px auto 360px', // TODO: will change CSS based on screen devices,
    color: theme?.color.bluegrey,
    boxSizing: 'border-box',
    fontWeight: theme?.fontWeight.medium,
    fontSize: theme?.fontSize.normal,
    margin: '0 auto',
    background: '#fff',
    height: '100vh',
    // maxWidth: '1440px',
  }),
);
