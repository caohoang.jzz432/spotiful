import { createTheme } from '@mui/material/styles';
import { CustomThemeOptions } from 'types/mui.type';
import { theme } from './theme.const';

/**
 * Global theme to be used in React Component
 */
// eslint-disable-next-line @typescript-eslint/no-unsafe-argument
export const castedGlobalTheme = createTheme(
  // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
  theme,
) as unknown as CustomThemeOptions;
