/* eslint-disable @typescript-eslint/no-empty-interface */
import { createTheme } from '@mui/material/styles';
import { theme } from './theme.const';

/**
 * User-defined props passed down to styled components
 */
const allUserDefinedProps = [
  '$size',
  '$border',
  '$width',
  '$height',
  '$active',
  '$longText',
  '$anchor',
  '$absolute',
  '$color',
  '$align',
  '$bgcolor',
  '$autoFill',
  '$thumbColor',
];

/**
 * Stop @emotion/styled from passing down user-defined props to React.Node or DOM element
 * https://stackoverflow.com/a/69088212
 */
export const customStyledOptions = {
  shouldForwardProp: (props: string) => !allUserDefinedProps.includes(props),
};

/**
 *
 */
// eslint-disable-next-line @typescript-eslint/no-unsafe-argument
export const globalTheme = createTheme(theme);
