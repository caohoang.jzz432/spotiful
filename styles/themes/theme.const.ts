/**
 * Theme-related properties
 */
export const color = {
  bluegrey: '#3e466c',
  orange: '#fca451',
  lightgrey: '#9ca9c7', // secondary text
  greywhite: '#91bfc1', // background
  greyborder: '#ecf3fc',
  bluegradient: 'linear-gradient(to right, #7f7fd5, #86a8e7, #91eae4)',
  white: '#fff',
  whitetransparent: 'rgba(255, 255, 255, 0.3)',
  whiteboldtransparent: 'rgba(255, 255, 255, 0.7)',
};

export const fontSize = {
  small: '12px',
  normal: '14px',
  medium: '16px',
  large: '24px',
};

export const fontWeight = {
  normal: '400',
  medium: '600',
  large: '800',
};

export const borderRadius = {
  none: '0',
  card: '10px',
  curved: '15px',
  round: '100rem',
};

export const imageSize = {
  normal: '35px',
  medium: '50px',
  extraMedium: '80px',
  large: '120px',
  extraLarge: '135px',
};

export const align = {
  left: 'left',
  center: 'center',
  right: 'right',
};

export const shadow = {
  shadowHover: '1px 1px 12px -5px rgba(0,0,0,0.48)',
};

/**
 * Global theme object shared to all styled components
 */
export const theme: any = {
  color,
  fontSize,
  fontWeight,
  borderRadius,
  imageSize,
  align,
  shadow,
};
