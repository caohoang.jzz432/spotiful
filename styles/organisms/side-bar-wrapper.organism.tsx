import { styled } from '@mui/system';
import { OverriddenMUIStyledCommonProps } from '../../types/mui.type';

export const SSideBarWrapper = styled('div')(({ theme }: OverriddenMUIStyledCommonProps) => ({
  borderLeft: `1px solid ${theme?.color.greyborder || ''}`,
}));
