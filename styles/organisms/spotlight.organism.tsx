import { styled } from '@mui/system';

export const SSpotlight = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  overflowX: 'hidden',
  overflowY: 'auto',
  width: '100%',
  padding: '0 40px',
  height: 'calc(100vh - 70px)',
  gap: '20px 0',
  paddingBottom: '20px',

  color: 'rgba(0, 0, 0, 0)',
  transition: 'color .3s ease',

  '&:hover': {
    color: 'rgba(24, 33, 53, 0.5)',
  },

  '&::-webkit-scrollbar,&::-webkit-scrollbar-thumb': {
    width: 4,
    borderRadius: 13,
  },

  '&::-webkit-scrollbar-thumb': {
    boxShadow: 'inset 0 0 0 10px',
  },
});
