import { styled } from '@mui/system';
import { OverriddenMUIStyledCommonProps } from 'types/mui.type';

export const SNavBarWrapper = styled('div')(
  ({ theme }: OverriddenMUIStyledCommonProps) => ({
    position: 'relative',
    borderRight: `1px solid ${theme?.color.greyborder || ''}`,
  }),
);
