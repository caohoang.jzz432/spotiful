import { styled } from '@mui/system';

export const SSideBar = styled('div')(() => ({
  display: 'flex',
  flexDirection: 'column',
  gap: '25px 0',
  margin: '0 40px',
}));
