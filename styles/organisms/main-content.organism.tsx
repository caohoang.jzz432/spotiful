import { styled } from '@mui/system';

export const SMainContent = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  gap: '20px 0',
});
