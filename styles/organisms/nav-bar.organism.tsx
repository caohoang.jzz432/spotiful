import { styled } from '@mui/system';

export const SNavBar = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  gap: '35px 0',
  marginLeft: '40px',
});
