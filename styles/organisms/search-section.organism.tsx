import { styled } from '@mui/system';

export const SSearchSection = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  gap: '20px 0',
});
