import { CssBaseline } from '@mui/material';
import Document, { Head, Html, Main, NextScript } from 'next/document';

/**
 * To custom default document of Next.js
 *
 * Support for Material UI's default font
 * Support for Stylead Component with Babel Compiler
 */
class CustomDocument extends Document {
  render() {
    return (
      <Html>
        <Head>
          {/* Default font for Material UI */}

          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
          />

          {/* Default font for Material Icon */}

          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/icon?family=Material+Icons"
          />
        </Head>

        <body>
          {/* Material UI. Fix common CSS problems between browsers, make CSS cross-compability better */}

          <CssBaseline />

          <Main />

          <NextScript />
        </body>
      </Html>
    );
  }
}

export default CustomDocument;
