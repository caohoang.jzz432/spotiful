import { Layout } from 'components/templates/layout.component';
import type { AppProps } from 'next/app';
import Head from 'next/head';
import 'styles/main.css';

/** Place shared layout that is seamlessly used througout the app here */
const CustomApp = function ({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        {/* Material UI Responsive meta tag */}

        <title>Spotiful ー Better Music Streaming</title>

        <meta name="viewport" content="initial-scale=1, width=device-width" />
      </Head>

      <Layout className="main-layout">
        <Component {...pageProps} />
      </Layout>
    </>
  );
};

export default CustomApp;
