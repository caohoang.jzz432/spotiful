import { Box } from '@mui/system';
import Image from 'next/image';
import { SLogo } from 'styles/atoms/logo.atom';
import spotifulLogo from 'public/logos/spotiful-64.png';

export const Logo = function () {
  return (
    <SLogo className="logo">
      <Box sx={{ width: 32, height: 32 }}>
        <Image src={spotifulLogo} alt="Better Music Streaming" quality={100} />
      </Box>

      <span className="nav-bar__brand-name">{process.env.APP_NAME}</span>
    </SLogo>
  );
};
