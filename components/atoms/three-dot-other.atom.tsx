import CircleIcon from '@mui/icons-material/Circle';
import { SThreeDotOther } from 'styles/atoms/three-dot-other.atom';

export const ThreeDotOther = function () {
  return (
    <SThreeDotOther>
      <CircleIcon sx={{ fontSize: '5px' }} />

      <CircleIcon sx={{ fontSize: '5px' }} />

      <CircleIcon sx={{ fontSize: '5px' }} />
    </SThreeDotOther>
  );
};
