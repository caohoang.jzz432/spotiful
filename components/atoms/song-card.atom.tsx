import Image from 'next/image';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import PauseIcon from '@mui/icons-material/Pause';
import albumCover from 'public/album-cover.jpg';
import { SStrictImageWrapper } from 'styles/atoms/image.atom';
import { SSongCard } from 'styles/molecules/song-card.molecule';
import { SquareButton } from 'components/atoms/square-button.atom';
import { STypography } from 'styles/atoms/typography.atom';
import { SongCardProps } from 'types/atomic/atom.type';

export const SongCard = function ({ songName, artist }: SongCardProps) {
  const isPlaying = true;

  return (
    <SSongCard className="card">
      <SStrictImageWrapper className="card-cover" $autoFill>
        <Image src={albumCover} alt="Stoney Album" quality={100} />

        <SquareButton
          icon={
            isPlaying ? (
              <PauseIcon fontSize="small" />
            ) : (
              <PlayArrowIcon fontSize="small" />
            )
          }
          color="orange"
          absolute
        />
      </SStrictImageWrapper>

      <STypography className="card-title" title="Go Flex" $longText $anchor>
        {songName}
      </STypography>

      <STypography
        className="card-artist"
        $color="lightgrey"
        $size="small"
        title="Post Malone"
        $longText
        $anchor
      >
        {artist}
      </STypography>
    </SSongCard>
  );
};
