import { ReactElement } from 'react';
import { SSquareButton } from 'styles/atoms/square-button.atom';
import { SquareButtonProps } from 'types/atomic/atom.type';
import { ColorKey } from 'types/mui.type';

export const SquareButton = function ({
  icon,
  color,
  absolute = false,
  border = false,
  className,
}: SquareButtonPropsttonProps) {
  return (
    <SSquareButton
      className={`square-button ${className || ''}`}
      $color={color}
      $absolute={absolute}
      $border={border}
    >
      {icon}
    </SSquareButton>
  );
};
