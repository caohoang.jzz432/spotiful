import { SStrictImageWrapper } from 'styles/atoms/image.atom';
import { SPickArtist } from 'styles/atoms/top-artist.atom';
import { STypography } from 'styles/atoms/typography.atom';
import imageCover from 'public/album-cover.jpg';
import Image from 'next/image';
import { Chip } from '@mui/material';
import { BaseProps } from 'types/react.type';
import { castedGlobalTheme } from 'styles/themes/casted-global.theme';

export const PickArtist = function ({ className }: BaseProps) {
  return (
    <SPickArtist className={className}>
      <SStrictImageWrapper
        className="artist-cover"
        $border="round"
        $size="extraMedium"
      >
        <Image src={imageCover} quality={100} />
      </SStrictImageWrapper>

      <STypography $color="whiteboldtransparent" sx={{ textAlign: 'center' }}>
        Post Malone
      </STypography>

      <Chip
        label="Artist"
        sx={{
          width: '70px',
          color: castedGlobalTheme.color.whiteboldtransparent,
        }}
      />
    </SPickArtist>
  );
};
