import { Slider } from '@mui/material';
import { useCallback, useState } from 'react';
import { SMusicSlider } from 'styles/atoms/music-slider.atom';
import { STypography } from 'styles/atoms/typography.atom';
import { MusicSliderProps } from 'types/atomic/atom.type';
import { getMusicTime } from 'utils/get-music-time.util';

export const MusicSlider = function ({
  duration = 100,
  thumbColor = 'transparent',
}: MusicSliderProps) {
  const [sliderValue, setSliderValue] = useState<any>(50);

  const totalTime = getMusicTime(duration);

  // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
  const currentTime = getMusicTime(sliderValue);

  const onChangeHandler = useCallback((_, value) => {
    setSliderValue(value);
  }, []);

  return (
    <SMusicSlider $thumbColor={thumbColor}>
      <STypography $color="white">{currentTime}</STypography>

      <Slider
        size="small"
        value={sliderValue}
        max={duration}
        onChange={onChangeHandler}
      />

      <STypography $color="white">{totalTime}</STypography>
    </SMusicSlider>
  );
};
