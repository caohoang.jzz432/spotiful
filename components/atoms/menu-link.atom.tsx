import React, { ReactElement } from 'react';
import { SMenuLink } from 'styles/atoms/menu-link.atom';
import { castedGlobalTheme } from 'styles/themes/casted-global.theme';
import { MenuLinkProps } from 'types/atomic/atom.type';

const { color } = castedGlobalTheme;

export const MenuLink = function ({ icon, title, active }: MenuLinkProps) {
  return (
    <SMenuLink className="menu-link" $active={active}>
      {React.cloneElement(icon, {
        fontSize: 'medium',
        htmlColor: color.lightgrey,
        className: 'menu-icon',
      })}

      {title}
    </SMenuLink>
  );
};
