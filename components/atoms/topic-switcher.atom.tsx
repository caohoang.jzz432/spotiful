import { Link } from '@mui/material';
import { STopicSwitcherWrapper } from '../../styles/atoms/topic-switcher-wrapper.atom';
import { STopicSwitcher } from '../../styles/atoms/topic-switcher.atom';

export const TopicSwitcher = function () {
  return (
    <STopicSwitcherWrapper className="topic-switcher">
      <STopicSwitcher>
        <Link className="anchor" href="/api">
          Music
        </Link>

        <Link className="anchor" href="/api">
          Podcast
        </Link>

        <Link className="anchor" href="/api">
          Live
        </Link>

        <Link className="anchor" href="/api">
          Radio
        </Link>
      </STopicSwitcher>
    </STopicSwitcherWrapper>
  );
};
