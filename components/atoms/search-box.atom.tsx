import SearchIcon from '@mui/icons-material/Search';
import { Input, InputAdornment } from '@mui/material';
import { SSSearchBox } from 'styles/atoms/search-box.atom';

export const SearchBox = function () {
  return (
    <SSSearchBox className="search-box">
      <Input
        startAdornment={
          <InputAdornment position="start">
            <SearchIcon />
          </InputAdornment>
        }
        placeholder="Artists, songs or podcasts"
        fullWidth
      />
    </SSSearchBox>
  );
};
