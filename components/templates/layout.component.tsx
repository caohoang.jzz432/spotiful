import { ThemeProvider } from '@mui/material/styles';
import { MainContent } from 'components/organisms/main-content.component';
import { NavBar } from 'components/organisms/nav-bar.component';
import { SideBar } from 'components/organisms/side-bar.component';
import { SLayout } from 'styles/templates/layout.template';
import { globalTheme } from 'styles/themes/global.theme';
import { BaseProps } from 'types/react.type';

export const Layout = function ({ children }: BaseProps) {
  return (
    <ThemeProvider theme={globalTheme}>
      <SLayout className="layout">
        <NavBar />

        <MainContent>{children}</MainContent>

        <SideBar />
      </SLayout>
    </ThemeProvider>
  );
};
