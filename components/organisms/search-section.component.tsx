import { styled } from '@mui/system';
import { PickArtist } from 'components/atoms/pick-artist.atom';
import { SearchBox } from 'components/atoms/search-box.atom';
import { InterestBox } from 'components/molecules/interest-box.molecule';
import { LightSongBox } from 'components/molecules/light-song-box.molecule';
import { mockData } from 'components/molecules/recent-played-box.molecule';
import { STypography } from 'styles/atoms/typography.atom';
import { STopResult } from 'styles/molecules/top-result.molecule';
import { SSearchSection } from 'styles/organisms/search-section.organism';
import { OverriddenMUIStyledCommonProps } from 'types/mui.type';

const STopResultWrapper = styled('div')(
  ({ theme }: OverriddenMUIStyledCommonProps) => ({
    display: 'flex',
    flexDirection: 'column',
    gap: '20px 0',
    padding: '15px',
    borderRadius: theme?.borderRadius.card,
    border: `1px solid ${theme?.color.greyborder || ''}`,
  }),
);

const SSearchContent = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  overflowX: 'hidden',
  overflowY: 'auto',
  width: '100%',
  height: 'calc(100vh - 80px)',
  gap: '20px 0',

  padding: '0 40px',
  paddingBottom: '20px',

  color: 'rgba(0, 0, 0, 0)',
  transition: 'color .3s ease',

  '&:hover': {
    color: 'rgba(24, 33, 53, 0.5)',
  },

  '&::-webkit-scrollbar,&::-webkit-scrollbar-thumb': {
    width: 4,
    borderRadius: 13,
  },

  '&::-webkit-scrollbar-thumb': {
    boxShadow: 'inset 0 0 0 10px',
  },
});

export const SearchSection = function () {
  return (
    <SSearchSection className="search-section">
      <SearchBox />

      <SSearchContent className="search-content">
        <STopResultWrapper className="box-container">
          <STypography>Top Result</STypography>

          <STopResult className="top-result">
            <PickArtist className="pick-artist" />

            <LightSongBox data={mockData} />
          </STopResult>
        </STopResultWrapper>

        <InterestBox title="Global Top 50" />

        <InterestBox title="Global Top 50" />
      </SSearchContent>
    </SSearchSection>
  );
};
