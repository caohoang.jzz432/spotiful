import { TopicSwitcher } from 'components/atoms/topic-switcher.atom';
import { InterestBox } from 'components/molecules/interest-box.molecule';
import { RecentPlayedBox } from 'components/molecules/recent-played-box.molecule';
import { SMainContent } from 'styles/organisms/main-content.organism';
import { SSpotlight } from 'styles/organisms/spotlight.organism';
import { BaseProps } from 'types/react.type';
import { SearchSection } from './search-section.component';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const MainContent = function (props: BaseProps) {
  const isSearchSection = true;

  return (
    <SMainContent className="main-content mt-7">
      {isSearchSection ? (
        <SearchSection />
      ) : (
        <>
          <TopicSwitcher />

          <SSpotlight className="spotlight">
            <InterestBox title="Global Top 50" />

            <InterestBox title="Global Top 50" />

            <RecentPlayedBox title="Recent Played Box" />

            <RecentPlayedBox title="Recent Played Box" />

            <RecentPlayedBox title="Recent Played Box" />
          </SSpotlight>
        </>
      )}
    </SMainContent>
  );
};
