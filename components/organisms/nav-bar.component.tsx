import HomeIcon from '@mui/icons-material/Home';
import { SNavBarWrapper } from 'styles/organisms/nav-bar-wrapper.organism';
import { SNavBar } from 'styles/organisms/nav-bar.organism';
import { SNavBarCopyright } from 'styles/atoms/nav-bar-copyright.atom';
import { Logo } from 'components/atoms/logo.atom';
import { MenuGroup } from 'components/molecules/menu-group.molecule';
import { MenuLink } from 'components/atoms/menu-link.atom';

export const NavBar = function () {
  return (
    <SNavBarWrapper className="nav-bar-wrapper">
      <SNavBar className="nav-bar mt-7">
        <Logo />

        <MenuGroup title="MENU">
          <MenuLink key="test1" icon={<HomeIcon />} title="Home" />

          <MenuLink key="test2" icon={<HomeIcon />} title="Home" />

          <MenuLink key="test3" icon={<HomeIcon />} title="Home" />

          <MenuLink key="test4" icon={<HomeIcon />} title="Home" />
        </MenuGroup>

        <MenuGroup title="MENU">
          <MenuLink key="test1" icon={<HomeIcon />} title="Home" />

          <MenuLink key="test2" icon={<HomeIcon />} title="Home" />

          <MenuLink key="test3" icon={<HomeIcon />} title="Home" />

          <MenuLink key="test4" icon={<HomeIcon />} title="Home" />
        </MenuGroup>

        <MenuGroup className="menu-group" title="MENU">
          <MenuLink key="test1" icon={<HomeIcon />} title="Home" />

          <MenuLink key="test2" icon={<HomeIcon />} title="Log Out" />
        </MenuGroup>

        <SNavBarCopyright className="copyright">
          © 2021 🎉 Made by me
        </SNavBarCopyright>
      </SNavBar>
    </SNavBarWrapper>
  );
};
