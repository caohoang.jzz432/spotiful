import { MusicPlayer } from 'components/molecules/music-player.molecule';
import { NotificationWrapper } from 'components/molecules/notification-wrapper.molecule';
import { TopArtistWrapper } from 'components/molecules/top-artitst-wrapper.molecule';
import { SSideBarWrapper } from 'styles/organisms/side-bar-wrapper.organism';
import { SSideBar } from 'styles/organisms/side-bar.organism';

export const SideBar = function () {
  return (
    <SSideBarWrapper className="side-bar-wrapper">
      <SSideBar className="side-bar mt-7">
        <NotificationWrapper />

        <TopArtistWrapper />

        <MusicPlayer />
      </SSideBar>
    </SSideBarWrapper>
  );
};
