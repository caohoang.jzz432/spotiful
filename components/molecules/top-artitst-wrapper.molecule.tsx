import { styled } from '@mui/system';
import { ThreeDotOther } from 'components/atoms/three-dot-other.atom';
import { STypography } from 'styles/atoms/typography.atom';
import { STitleBox } from 'styles/molecules/title-box.molecule';
import { GoodBox } from './good-box.molecule';

const STopArtistWrapper = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  gap: '20px 0',
});

export const TopArtistWrapper = function () {
  return (
    <STopArtistWrapper className="top-artist-wrapper">
      <STitleBox className="title-box">
        <STypography $longText $size="normal">
          Top Artists
        </STypography>

        <ThreeDotOther />
      </STitleBox>

      <GoodBox />
    </STopArtistWrapper>
  );
};
