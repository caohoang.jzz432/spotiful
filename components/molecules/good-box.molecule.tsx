import { styled } from '@mui/system';
import Image from 'next/image';
import { SStrictImageWrapper } from 'styles/atoms/image.atom';
import { STypography } from 'styles/atoms/typography.atom';
import albumCover from 'public/album-cover.jpg';
import { GoodBoxData } from 'types/atomic/molecule.type';

const STopArtistBox = styled('div')({
  display: 'grid',
  gridTemplateColumns: 'repeat(auto-fill, minmax(55px, 1fr))',
  gap: '20px 20px',
});

const STopArtist = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  gap: '5px 0',
  alignItems: 'center',
  cursor: 'pointer',
  '& .artist-name': {
    maxWidth: '70px',
  },
});

const mockData: GoodBoxData[] = [
  {
    imageCover: albumCover,
    title: 'Hieuthuhai',
  },
  {
    imageCover: albumCover,
    title: 'Hieuthuhai',
  },
  {
    imageCover: albumCover,
    title: 'Hieuthuhai',
  },
  {
    imageCover: albumCover,
    title: 'Hieuthuhai',
  },
  {
    imageCover: albumCover,
    title: 'Hieuthuhai',
  },
  {
    imageCover: albumCover,
    title: 'Hieuthuhai',
  },
];

export const GoodBox = function () {
  return (
    <STopArtistBox className="top-artist-box">
      {mockData.map((item, index) => (
        // eslint-disable-next-line react/no-array-index-key
        <STopArtist key={index} className="top-artist">
          <SStrictImageWrapper
            className="artist-cover"
            $autoFill
            $border="round"
          >
            <Image src={item.imageCover} quality={100} />
          </SStrictImageWrapper>

          <STypography
            className="artist-name"
            $longText
            $size="small"
            $align="center"
          >
            {item.title}
          </STypography>
        </STopArtist>
      ))}
    </STopArtistBox>
  );
};
