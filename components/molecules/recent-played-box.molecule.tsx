import { STypography } from 'styles/atoms/typography.atom';
import { SRecentPlayedBox } from 'styles/molecules/recent-played-box.molecule';
import { STitleBox } from 'styles/molecules/title-box.molecule';
import albumCover from 'public/album-cover.jpg';
import { RecentPlayedBoxProps, SongBoxData } from 'types/atomic/molecule.type';
import { SongBox } from './song-box.molecule';

export const mockData: SongBoxData[] = [
  {
    index: 1,
    imageCover: albumCover,
    songName: 'I Miss You',
    artist: 'Blink 182',
    genre: 'Dark',
    duration: 300,
  },
  {
    index: 2,
    imageCover: albumCover,
    songName: 'I Miss You',
    artist: 'Blink 182',
    genre: 'Dark',
    duration: 300,
  },
  {
    index: 3,
    imageCover: albumCover,
    songName: 'I Miss You',
    artist: 'Blink 182',
    genre: 'Dark',
    duration: 300,
  },
];

export const RecentPlayedBox = function ({ title }: RecentPlayedBoxProps) {
  return (
    <SRecentPlayedBox className="recent-played-box">
      <STitleBox className="title-box">
        <STypography $size="normal">{title}</STypography>

        <STypography $size="normal" $color="lightgrey" $anchor>
          See all
        </STypography>
      </STitleBox>

      <SongBox data={mockData} />
    </SRecentPlayedBox>
  );
};
