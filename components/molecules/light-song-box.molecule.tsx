import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import { SquareButton } from 'components/atoms/square-button.atom';
import Image from 'next/image';
import { SStrictImageWrapper } from 'styles/atoms/image.atom';
import { STypography } from 'styles/atoms/typography.atom';
import { SLightSongBox } from 'styles/molecules/light-song-box.molecule';
import { STitleBox } from 'styles/molecules/title-box.molecule';
import { SongBoxProps } from 'types/atomic/molecule.type';
import { getMusicTime } from 'utils/get-music-time.util';
import { styled } from '@mui/system';
import { SSong, SSongControl, SSongDetail } from './song-box.molecule';

const SSongBox = styled('div')({
  display: 'grid',
  gridTemplateColumns: '4fr 1fr 1fr',
  gap: '0 16px',
  alignItems: 'center',
});

export const LightSongBox = function ({ data }: SongBoxProps) {
  return (
    <SLightSongBox className="light-song-box">
      <STitleBox className="title-box">
        <STypography $size="normal">Songs</STypography>

        <STypography $size="normal" $color="lightgrey" $anchor>
          See all
        </STypography>
      </STitleBox>

      {data.map((item) => (
        <SSongBox key={item.index} className="song-box">
          <SSong className="song">
            <SStrictImageWrapper
              className="song-image"
              $size="normal"
              $border="card"
            >
              <Image src={item.imageCover} quality={100} />
            </SStrictImageWrapper>

            <SSongDetail className="song-detail">
              <STypography
                className="song-name"
                $longText
                title="Hello Brother"
                $anchor
              >
                {item.songName}
              </STypography>

              <STypography
                $color="lightgrey"
                $size="small"
                className="song-artist"
                $longText
                title="Hello Brother"
                $anchor
              >
                {item.artist}
              </STypography>
            </SSongDetail>
          </SSong>

          <STypography>{getMusicTime(item.duration)}</STypography>

          <SSongControl className="song-control">
            <SquareButton
              className="play-button"
              icon={<PlayArrowIcon fontSize="small" />}
              color="orange"
              border
            />
          </SSongControl>
        </SSongBox>
      ))}
    </SLightSongBox>
  );
};
