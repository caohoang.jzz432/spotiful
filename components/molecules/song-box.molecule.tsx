import { styled } from '@mui/system';
import { SStrictImageWrapper } from 'styles/atoms/image.atom';
import { STypography } from 'styles/atoms/typography.atom';
import Image from 'next/image';
import { SquareButton } from 'components/atoms/square-button.atom';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import AddIcon from '@mui/icons-material/Add';
import { SongBoxProps } from 'types/atomic/molecule.type';
import { getMusicTime } from 'utils/get-music-time.util';
import { getUsefulIndex } from 'utils/get-useful-index.util';

export const SSongBoxHeader = styled('div')({
  display: 'grid',
  gridTemplateColumns: '16px 3fr minmax(60px,2fr) 60px 2fr',
  gap: '0 16px',
  alignItems: 'center',
});

const SSongBox = styled('div')({
  display: 'grid',
  gridTemplateColumns: '16px 3fr minmax(60px,2fr) 60px 2fr',
  gap: '0 16px',
  alignItems: 'center',
});

export const SSong = styled('div')({
  display: 'flex',
  gap: '0 20px',
  alignItems: 'center',

  '& .song-image': {
    cursor: 'pointer',
  },
});

export const SSongDetail = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  maxWidth: '240px',
});

export const SSongControl = styled('div')({
  display: 'flex',
  gap: '0 20px',
  alignItems: 'center',
  justifyContent: 'center',
});

export const SongBox = function ({ data }: SongBoxProps) {
  return (
    <>
      <SSongBoxHeader className="song-box">
        <STypography $color="lightgrey">#</STypography>

        <STypography $color="lightgrey">Song</STypography>

        <STypography $color="lightgrey">Album</STypography>

        <STypography $color="lightgrey">Duration</STypography>
      </SSongBoxHeader>

      {data.map((item) => (
        <SSongBox key={item.index} className="song-box">
          <STypography $color="lightgrey">
            {getUsefulIndex(item.index)}
          </STypography>

          <SSong className="song">
            <SStrictImageWrapper
              className="song-image"
              $size="normal"
              $border="card"
            >
              <Image src={item.imageCover} quality={100} />
            </SStrictImageWrapper>

            <SSongDetail className="song-detail">
              <STypography
                className="song-name"
                $longText
                title="Hello Brother"
                $anchor
              >
                {item.songName}
              </STypography>

              <STypography
                $color="lightgrey"
                $size="small"
                className="song-artist"
                $longText
                title="Hello Brother"
                $anchor
              >
                {item.artist}
              </STypography>
            </SSongDetail>
          </SSong>

          <STypography $anchor $longText>
            {item.genre}
          </STypography>

          <STypography>{getMusicTime(item.duration)}</STypography>

          <SSongControl className="song-control">
            <SquareButton
              className="play-button"
              icon={<PlayArrowIcon fontSize="small" />}
              color="orange"
              border
            />

            <SquareButton
              className="add-button"
              icon={<AddIcon fontSize="small" />}
              color="bluegrey"
              border
            />
          </SSongControl>
        </SSongBox>
      ))}
    </>
  );
};
