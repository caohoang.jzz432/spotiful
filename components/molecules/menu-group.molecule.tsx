import { SMenuGroupHeader } from 'styles/atoms/menu-group-header.atom';
import { SMenuGroup } from 'styles/molecules/menu-group.molecule';
import { MenuGroupProps } from 'types/atomic/molecule.type';

export const MenuGroup = function ({ children, title }: MenuGroupProps) {
  return (
    <SMenuGroup className="menu-group">
      <SMenuGroupHeader>{title}</SMenuGroupHeader>

      {children}
    </SMenuGroup>
  );
};
