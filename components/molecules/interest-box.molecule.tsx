import { STypography } from 'styles/atoms/typography.atom';
import { SInterestBox } from 'styles/molecules/interest-box.molecule';
import { STitleBox } from 'styles/molecules/title-box.molecule';
import { InterestBoxProps } from 'types/atomic/molecule.type';
import { CardBox } from './card-box.molecule';

export const InterestBox = function ({ title }: InterestBoxProps) {
  return (
    <SInterestBox className="interest-box">
      <STitleBox>
        <STypography $size="normal">{title}</STypography>

        <STypography $size="normal" $color="lightgrey" $anchor>
          See all
        </STypography>
      </STitleBox>

      <CardBox />
    </SInterestBox>
  );
};
