import { useLimitOnResize } from 'hooks/use-limit-on-resize.hook.hook';
import { SCardBox } from 'styles/molecules/card-box.molecule';
import { SongCardData } from 'types/atomic/atom.type';
import { SongCard } from '../atoms/song-card.atom';

export const CardBox = function () {
  const mockData: SongCardData[] = [
    {
      songName: 'Go Flex',
      artist: 'Post Malone',
    },
    {
      songName: 'Go Flex',
      artist: 'Post Malone',
    },
    {
      songName: 'Go Flex',
      artist: 'Post Malone',
    },
    {
      songName: 'Go Flex',
      artist: 'Post Malone',
    },
    {
      songName: 'Go Flex',
      artist: 'Post Malone',
    },
    {
      songName: 'Go Flex',
      artist: 'Post Malone',
    },
  ];

  const { limit } = useLimitOnResize({
    childClassName: '.card',
    parentClassName: '.card-box',
    total: mockData.length || 0,
  });

  return (
    <SCardBox className="card-box">
      {mockData.slice(0, limit).map((item, index) => (
        // eslint-disable-next-line react/no-array-index-key
        <SongCard key={index} songName={item.songName} artist={item.artist} />
      ))}
    </SCardBox>
  );
};
