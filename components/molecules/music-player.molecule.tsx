import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import RepeatIcon from '@mui/icons-material/Repeat';
import ShuffleIcon from '@mui/icons-material/Shuffle';
import SkipNextIcon from '@mui/icons-material/SkipNext';
import SkipPreviousIcon from '@mui/icons-material/SkipPrevious';
import { styled } from '@mui/system';
import { MusicSlider } from 'components/atoms/music-slider.atom';
import { useDominantColor } from 'hooks/use-dominant-color.hook';
import Image from 'next/image';
import albumCover2 from 'public/album-cover-2.jpg';
import { SStrictImageWrapper } from 'styles/atoms/image.atom';
import {
  SMusicControl,
  SMusicControlContainer,
} from 'styles/atoms/music-control.atom';
import { STypography } from 'styles/atoms/typography.atom';
import { SMusicPlayer } from 'styles/molecules/music-player.molecule';

const SPlayingSongDetail = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  gap: '5px 0',
  alignItems: 'center',
});

export const MusicPlayer = function () {
  const dominantColor = useDominantColor('/album-cover-2.jpg');

  return (
    <SMusicPlayer className="music-player" $bgcolor={dominantColor}>
      <STypography $align="center" $color="white">
        Now Playing
      </STypography>

      <SStrictImageWrapper $size="extraLarge" $border="card">
        <Image src={albumCover2} quality={100} />
      </SStrictImageWrapper>

      <SPlayingSongDetail>
        <STypography
          className="card-title"
          title="Go Flex"
          $color="white"
          $longText
          $anchor
        >
          Go Flex
        </STypography>

        <STypography
          className="card-artist"
          $color="whitetransparent"
          $size="small"
          title="Post Malone"
          $longText
          $anchor
        >
          Post Malone
        </STypography>
      </SPlayingSongDetail>

      <MusicSlider duration={100} thumbColor={dominantColor} />

      <SMusicControlContainer>
        <SMusicControl>
          <RepeatIcon fontSize="small" />
        </SMusicControl>

        <SMusicControl>
          <SkipPreviousIcon fontSize="small" />
        </SMusicControl>

        <SMusicControl $active>
          <PlayArrowIcon fontSize="small" />
        </SMusicControl>

        <SMusicControl>
          <SkipNextIcon fontSize="small" />
        </SMusicControl>

        <SMusicControl>
          <ShuffleIcon fontSize="small" />
        </SMusicControl>
      </SMusicControlContainer>
    </SMusicPlayer>
  );
};
