import { styled } from '@mui/system';
import Image from 'next/image';
import { SStrictImageWrapper } from 'styles/atoms/image.atom';
import { STypography } from 'styles/atoms/typography.atom';
import { STitleBox } from 'styles/molecules/title-box.molecule';
import albumCover from 'public/album-cover.jpg';
import { ThreeDotOther } from 'components/atoms/three-dot-other.atom';

const SNotificationWrapper = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  gap: '20px 0',
});

const SNotificationBox = styled('div')({
  display: 'flex',
  justifyContent: 'space-between',
  cursor: 'pointer',
});

const SNotification = styled('div')({
  display: 'flex',
  gap: '0 20px',
});

const SNotificationDetail = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  maxWidth: '110px',
});

export const NotificationWrapper = function () {
  return (
    <SNotificationWrapper className="notification-wrapper">
      <STitleBox className="title-box">
        <STypography $longText $size="normal">
          Notifications
        </STypography>

        <ThreeDotOther />
      </STitleBox>

      <SNotificationBox className="notification-box">
        <SNotification className="notification">
          <SStrictImageWrapper $size="normal" $border="card">
            <Image src={albumCover} quality={100} />
          </SStrictImageWrapper>

          <SNotificationDetail className="notification-detail">
            <STypography $longText>I Miss You</STypography>

            <STypography $longText $color="lightgrey" $size="small">
              Blink 182
            </STypography>
          </SNotificationDetail>
        </SNotification>

        <STypography $longText $color="lightgrey">
          8 Hours
        </STypography>
      </SNotificationBox>

      <SNotificationBox className="notification-box">
        <SNotification className="notification">
          <SStrictImageWrapper $size="normal" $border="card">
            <Image src={albumCover} quality={100} />
          </SStrictImageWrapper>

          <SNotificationDetail className="notification-detail">
            <STypography $longText>I Miss You</STypography>

            <STypography $longText $color="lightgrey" $size="small">
              Blink 182
            </STypography>
          </SNotificationDetail>
        </SNotification>

        <STypography $longText $color="lightgrey">
          8 Hours
        </STypography>
      </SNotificationBox>
    </SNotificationWrapper>
  );
};
