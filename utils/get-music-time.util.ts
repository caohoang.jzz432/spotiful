/**
 *
 * @param duration duration in seconds
 * @returns 'minutes:seconds' format
 */
export function getMusicTime(duration: number) {
  const minutes = Math.floor(duration / 60);
  const seconds = duration - minutes * 60;

  return `${minutes <= 9 ? '0' + minutes : minutes}:${
    seconds <= 9 ? '0' + seconds : seconds
  }`;
}
