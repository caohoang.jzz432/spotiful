/**
 * @param index
 * @returns index string in '0xxx' format
 */
export const getUsefulIndex = function (index: number) {
  return `${index <= 9 ? `0${index}` : index}`;
};
