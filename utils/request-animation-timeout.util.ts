/**
 * Execute callback AFTER the UI is completed rendered on the screen.
 * @param callback
 */
export function requestAnimationTimeout(callback: () => void) {
  requestAnimationFrame(() => {
    setTimeout(callback, 0);
  });
}
