import { prominent } from 'color.js';
import { useEffect, useState } from 'react';

/**
 * 'useDominantColor' hook is used to get dominant color of an image
 * @param imageSrc path refers to location of the image
 * @returns the dominant color
 */
export function useDominantColor(imageSrc: string) {
  const [dominantColor, setDominantColor] = useState<any>('#fff');

  useEffect(() => {
    prominent(imageSrc, { format: 'hex', amount: 3 })
      .then((color) => {
        setDominantColor(color[1]);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [imageSrc]);

  return dominantColor;
}
