/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/naming-convention */
import { useEffect, useRef, useState } from 'react';

/**
 * 'useWindowSize' hook is used to get width and height of browser viewport.
 * @returns
 * an object contains:
 *
 * - height: height of viewport.W
 *
 * - width: width of viewport.
 */
export function useWindowSize() {
  const resizeHandlerRef = useRef<any>(null);

  const heightRef = useRef(0);
  const widthRef = useRef(0);

  const [_, setIsResized] = useState(false);

  useEffect(() => {
    /**
     * Because of SSR, window is undefined.
     * So we need to get intial viewport width
     * and height manually by using 'useEffect' hook
     */
    heightRef.current = window?.innerHeight;
    widthRef.current = window?.innerWidth;

    setIsResized((prev) => !prev);

    resizeHandlerRef.current = () => {
      heightRef.current = window?.innerHeight;
      widthRef.current = window?.innerWidth;

      setIsResized((prev) => !prev);
    };

    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    window.addEventListener('resize', resizeHandlerRef.current);

    return () => {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
      window.removeEventListener('resize', resizeHandlerRef.current);
    };
  }, []);

  return {
    height: heightRef.current,
    width: widthRef.current,
  };
}
