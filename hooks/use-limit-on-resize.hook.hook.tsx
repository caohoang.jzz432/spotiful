/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/naming-convention */
import { useEffect, useRef, useState } from 'react';
import { requestAnimationTimeout } from 'utils/request-animation-timeout.util';
import { useWindowSize } from './use-window-size.hook';

interface LimitOnResizeHookProps {
  childClassName: string;
  parentClassName: string;
  total: number;
}

/**
 * 'useHideOnResize' hook is used to hide items
 * when total width of items exceeds the width of the parent.
 *
 * Note: 'parentClassName' prop should be class name of immediating parent of the items.
 * @returns
 * an object contains:
 *
 * - limit: number of items should be rendered.
 *
 * - shouldHide: boolean flag indicates whether total
 *   width of items exceeds the width of the parent.
 *
 * - total: number of data items
 */
export function useLimitOnResize({
  childClassName,
  parentClassName,
  total,
}: LimitOnResizeHookProps) {
  const limitRef = useRef(total);
  const shouldLimitRef = useRef(false);

  const [_, setShouldRun] = useState(false);

  /**
   * Required hook
   * to catch viewport's 'resize' event
   */
  const { height, width } = useWindowSize();

  useEffect(
    () => {
      if (total === 0 || !height || !width) {
        return;
      }

      requestAnimationTimeout(() => {
        const child = document.querySelector(childClassName);
        const parent = document.querySelector(parentClassName);

        if (!child || !parent) {
          return;
        }

        let totalChildWidths = child.clientWidth * total;

        limitRef.current = total;
        shouldLimitRef.current = false;

        while (totalChildWidths > parent.clientWidth) {
          shouldLimitRef.current = true;

          totalChildWidths -= child.clientWidth;
        }

        limitRef.current = Math.floor(totalChildWidths / child.clientWidth);

        setShouldRun((prev) => !prev);
      });
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [height, width],
  );

  return {
    limit: limitRef.current,
    shouldLimit: shouldLimitRef.current,
  };
}
